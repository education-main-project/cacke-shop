import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IProduction } from 'src/app/models/IProduction';
import { ConfigService } from '../config/config.service';
import { Observable } from 'rxjs';
import { IPagination } from 'src/app/models/IPagination';
import { IDeleteAnswer } from 'src/app/models/IDeleteAnswer';
import { FilterMetadata } from 'primeng/api';
import { Router, UrlSerializer } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductionRestService {

  constructor(
    private http: HttpClient,
    private roter: Router,
    private setializer: UrlSerializer

  ) { }

  createProduct(data: FormData): Observable<IProduction> {
    return this.http.post<IProduction>(ConfigService.config.endPoints.production, data, {reportProgress: true});
  }
  listOfProduction(
    pagesize: number,
    first:number,
    filters:{[s: string]: FilterMetadata;} | undefined = undefined
  ): Observable<IPagination<IProduction[]>> {
    const fTmp: {[s: string]: FilterMetadata;} = {};

    let query = '';
    if (filters){
      Object.keys(filters).forEach((key)=>{
        fTmp[key] = filters[key].value;
      });
      const tree = this.roter.createUrlTree([''],{ queryParams: fTmp });
      query = this.setializer.serialize(tree);
      console.log('productionService - filter:', fTmp);
    }
    console.log('productionService - filter Query:', query.slice(1));
    return this.http.get<IPagination<IProduction[]>>(
      `${ConfigService.config.endPoints.production}/${pagesize}/${first}${query.substring(1)}`
    );
  }
  getProduct(id:string): Observable<IProduction>{
    return this.http.get<IProduction>(ConfigService.createURL('production', id));
  }
  updateProduct(id: string, data: FormData): Observable<IProduction>{
    return this.http.put<IProduction>(ConfigService.createURL('production',id), data);
  }
  deleteOne(id: string): Observable<IDeleteAnswer> {
    return this.http.delete<IDeleteAnswer>(ConfigService.createURL('production', id));
  }
}
