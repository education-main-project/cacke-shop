import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubTitleService {
  private _subTitle = new Subject<string>();
  readonly $subTitle = this._subTitle.asObservable();
  constructor() { }
  setSubTitle(newTitle: string): void {
    this._subTitle.next(newTitle);
  }
}
