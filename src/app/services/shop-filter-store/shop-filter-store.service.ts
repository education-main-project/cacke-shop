import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class ShopFilterStoreService {
  private filterSubject = new BehaviorSubject<string[]>([]);
  $filter = this.filterSubject.asObservable();
  constructor() {
    const storedString = window.localStorage.getItem(ConfigService.config.shopFilter.storageKey);
    if (storedString){
      this.filterSubject.next(JSON.parse(storedString));
    }else{
      this.filterSubject.next(['торт', 'пироженное', 'дисерт' ,'выпечка']);
    }
  }

  storeFilter(value: string[]): void {
    window.localStorage.setItem(ConfigService.config.shopFilter.storageKey, JSON.stringify(value));
    this.filterSubject.next(value);
  }
}
