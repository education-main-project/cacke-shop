import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IProduction } from 'src/app/models/IProduction';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class RecipesRestService {

  constructor(private http: HttpClient) { }
  getRecipes(): Observable<IProduction[]>{
    return this.http.get<IProduction[]>(ConfigService.createURL('cakeList', 'recipes'));
  }
}
