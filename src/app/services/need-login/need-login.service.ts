import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NeedLoginService {
  private needLogin = new Subject<boolean>();
  readonly $needLogin = this.needLogin.asObservable();
  constructor() { }

  doLogin(): void {
    this.needLogin.next(true);
  }
}
