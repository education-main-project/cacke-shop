import { Injectable } from '@angular/core';
import { ICardInfo, ICartProduct } from 'src/app/models/ICart';
import { BehaviorSubject } from 'rxjs';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private cartInfoSubject = new BehaviorSubject<ICardInfo> ({count:0, totalCoast:0})
  readonly $cartInfoSubject = this.cartInfoSubject.asObservable();
  private cartSubject = new BehaviorSubject<ICartProduct[]>([]);
  readonly $cartSubject = this.cartSubject.asObservable();

  constructor() {
    this.getStorage().then(dt=>{
      console.log('restored data',dt);
      this.cartSubject.next(dt);
    }).catch(err=>{
      console.error(err);
      this.cartSubject.next([]);
    });
    this.cartInfo().then( storage=> {
      this.cartInfoSubject.next(storage);
    });
  }
  findById(id:string): Promise<ICartProduct|undefined>{
    return new Promise<ICartProduct|undefined>((res,rej)=>{
      this.getStorage().then(storage=>{
        res(storage.find( it=>it.productID === id));
      }).catch(rej);
    });
  }
  removeByIndex(index: number): Promise<string>{
    return new Promise<string>((res,rej)=>{
      this.getStorage().then(storage=>{
        this.setStorage(storage.filter((it,_index)=>_index!==index)).then(res).catch(rej);
      }).catch(rej);
    });
  }

  removeById(id: string): Promise<string>{
    return new Promise<string>((res,rej)=>{
      this.getStorage().then(storage=>{
        this.setStorage(storage.filter((it)=>it.productID!==id)).then(res).catch(rej);
      }).catch(rej);
    });
  }

  updateQuantityByIndex(index: number, quantity:number): Promise<string>{
    return new Promise<string>((res,rej)=>{
      this.getStorage().then(storage=>{
        if (storage[index]){
          storage[index].quantity = quantity;
          this.setStorage(storage).then(res).catch(rej);
        }else{
          rej(`index = "${index}" - не найден`);
        }
      }).catch(rej);
    });
  }

  updateQuantityById(id: string, quantity:number): Promise<string>{
    return new Promise<string>((res,rej)=>{
      this.getStorage().then(storage=>{
        const index = storage.findIndex(it=>it.productID === id);
        if (index>-1){
          storage[index].quantity = quantity;
          this.setStorage(storage).then(res).catch(rej);
        }else{
          rej(`id = "${id}" - не найден`);
        }
      }).catch(rej);
    });
  }

  addProduct(id: string, name: string, coast: number, quantity: number, discont: number): Promise<string> {
    return new Promise<string>((res,rej)=>{
      this.getStorage().then( storage=>{
        const index = storage.findIndex( it=> it.productID === id);
        if (index>-1){
          storage[index].quantity += quantity;
        }else{
          storage.push({
            productID: id,
            name,
            coast,
            quantity,
            diskont:discont
          });
        }
        this.setStorage(storage).then(res).catch(rej);
      }).catch(res);
    });
  }

  cartInfo(): Promise<ICardInfo> {
    return new Promise<ICardInfo>((res,rej)=>{
      this.getStorage().then(dt=>{
        res(dt.reduce( (prevous,it)=>{
          prevous.count += it.quantity;
          if (it.diskont){
            prevous.totalCoast += it.quantity * Math.round((it.coast - (it.coast/100)*it.diskont)*100) / 100;
          }else{
            prevous.totalCoast += it.coast * it.quantity;
          }
          return prevous;
        }, <ICardInfo>{count:0, totalCoast:0}));
      }).catch(rej)
    });
  }

  getStorage(): Promise<ICartProduct[]> {
    return new Promise<ICartProduct[]>((res,rej)=>{
      try{
        const stored = window.localStorage.getItem(ConfigService.config.cart.storageKey);
        if (stored){
          res(JSON.parse(stored));
        }else{
          res([]);
        }
      }catch(err){
        console.log("CartService - ошибка получения.", err);
        rej("Ошибка получения.");
      }
    });
  }

  clear(): Promise<string> {
    return new Promise<string>((res,rej)=>{
      try{
        window.localStorage.removeItem(ConfigService.config.cart.storageKey);
        console.log('Item rmoved');
        this.cartSubject.next([]);
        this.cartInfo().then( storage=>{
          this.cartInfoSubject.next(storage);
          res('ok');
        }).catch(rej);
      }catch(err){
        console.log("CartService - ошибка сохранения.", err);
        rej("Ошибка сохранения.");
      }
    });
  }

  setStorage(data: ICartProduct[]): Promise<string> {
    return new Promise<string>((res,rej)=>{
      this.cartSubject.next(data);
      try{
        window.localStorage.setItem(ConfigService.config.cart.storageKey, JSON.stringify(data));
        this.cartInfo().then( storage=>{
          this.cartInfoSubject.next(storage);
          res('ok');
        }).catch(rej);
      }catch(err){
        console.log("CartService - ошибка сохранения.", err);
        rej("Ошибка сохранения.");
      }
    });
  }
}
