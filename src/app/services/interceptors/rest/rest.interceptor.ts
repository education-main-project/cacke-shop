import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class RestInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private router:Router,
    private messageService: MessageService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.token;
    let req = request;
    if (token){
      console.log("interceptor has token.");
      req = request.clone({
        headers: request.headers.set("Authorization", `Bearer ${token}`)
      });
    }else{
      console.log("interceptor no token.");
    }

    return next.handle(req).pipe(
      tap({
        error: (event)=>{
          if (this.router.routerState.snapshot.url !== '/users/login' && event.status === 401){
            console.log('intercepter error:',event);
            console.log(this.router.routerState.snapshot.url)
            this.messageService.add({severity:'error', summary:'Ошибка!', detail:'Требуется авторизация'});
            this.authService.logout();
            this.router.navigate(['/users/login']);
          }
        },
      })
    );

  }
}
