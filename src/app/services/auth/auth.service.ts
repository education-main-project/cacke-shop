import { UsersService } from './../users/users.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ILSUser, IShortUser, IUser, IUserInfo, IUserLogin, IUserRegistration } from 'src/app/models/IUsers';
import { ConfigService } from '../config/config.service';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private usersService: UsersService
  ) { }
  logout(): void {
    this.usersService.clearStorage();
    this.usersService.setUser(null);
  }
  signup(userData: IUserRegistration): Promise<IShortUser | null> {
    return new Promise<IShortUser | null>((resolve,reject)=>{
      console.log(ConfigService.createURL("users"))
      this.http.post<ILSUser>(ConfigService.createURL("users"), userData).subscribe({
        next:respUserData=>{
          console.log('signup sucsess', respUserData);
          this.usersService.setUser( respUserData )
          resolve(respUserData.user);
        },
        error:err=>{
          console.log('signup eror', err);
          reject(err);
        }
      })
    })
  }
  signin(userData: IUserLogin): Promise<IShortUser | null> {
    return new Promise<IShortUser | null> ((resolve,reject)=>{
      const url = ConfigService.createURL("users", userData.username);
      console.log(url);
      this.http.post<ILSUser>(url, userData).subscribe({
        next: uData=>{
          console.log('signup server positive answer:', uData);
          this.usersService.setUser(uData)
          resolve(uData.user);
        },
        error: (err:HttpErrorResponse) =>{
          console.log('signup server negaive answer:', err);
          reject(err.error);
        }
      });
    });
  }
  checkToken(): Observable<{ok:string}> {
    return this.http.post<{ok:string}>(ConfigService.createURL('users','check'), {});
  }

  get isAuthorized(): boolean {
    return !!this.usersService.user;
  }
  get isAdmin(): boolean {
    return this.isAuthorized && this.usersService.user?.role === "admin";
  }

  get token(): string {
    const _token = window.localStorage.getItem(ConfigService.config?.token.storageKey);
    if (_token){
      return _token;
    }else{
      return '';
    }
  }
}
