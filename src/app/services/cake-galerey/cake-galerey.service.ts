import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, take } from 'rxjs';
import { ICakeGalerey } from 'src/app/models/ICakeGalerey';
import { StaticCakeGalery } from 'src/app/static/CakeGalaray';

@Injectable({
  providedIn: 'root'
})
export class CakeGalereyService {


  constructor() { }

  galerayList(): Observable<ICakeGalerey[]>{
    return  new BehaviorSubject<ICakeGalerey[]>(StaticCakeGalery).pipe(take(1));
  }
}
