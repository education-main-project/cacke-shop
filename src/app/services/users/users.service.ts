import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ILSUser, IShortUser } from 'src/app/models/IUsers';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private _user: IShortUser|null;
  private __user = new BehaviorSubject<IShortUser | null>(null);
  readonly $user = this.__user.asObservable();
  constructor( ) {
    if (ConfigService.config){
      this.__user.subscribe(
        userData=>{
          console.log(userData);
          const storeUser = ConfigService.config.storeUser;
          if (userData===null && storeUser){
            const tmpUser = this.getUserFromStorage();
            if (tmpUser){
              this.__user.next( tmpUser );
            }
          }else{
            if( storeUser ){
              window.localStorage.setItem(ConfigService.config.user.storageKey, JSON.stringify( userData ));
            }
          }
          this._user = userData;
        }
      );
    }
  }
  rescanUser(): void {
    const tmpUser = this.getUserFromStorage();
    if (tmpUser){
      this.__user.next( tmpUser );
    }else{
      this.clearStorage();
    }
  }
  private getUserFromStorage(): IShortUser | null{
    const jsonString:string | null = window.localStorage.getItem(ConfigService.config.user.storageKey);
    if (jsonString){
      return JSON.parse(jsonString);
    }else{
      return null;
    }
  }

  private setToken(t: string){
    window.localStorage.setItem(ConfigService.config.token.storageKey, t);
  }

  clearStorage():void{
    window.localStorage.removeItem(ConfigService.config.token.storageKey);
    window.localStorage.removeItem(ConfigService.config.user.storageKey);
  }

  setUser( userData:ILSUser | null ){
    if (userData){
      window.localStorage.setItem(ConfigService.config.user.storageKey, JSON.stringify(userData.user));
      this.setToken(userData.access_token);
      this.__user.next(userData.user);
    }else{
      this.clearStorage();
      this.__user.next(null);
    }
  }
  get user(): IShortUser | null{
    if (this._user){
      return this._user;
    }else{
      if (ConfigService.config.storeUser){
        return this.getUserFromStorage();
      }else{
        return null;
      }
    }
  }
}
