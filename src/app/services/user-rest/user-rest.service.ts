import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser, IUserRegistration } from 'src/app/models/IUsers';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class UserRestService {

  constructor(private http: HttpClient) { }

  getUser(username: string): Observable<IUserRegistration>{
    return this.http.get<IUserRegistration>(ConfigService.createURL('users',username));
  }

  updateUser(id: string, userData: IUserRegistration): Observable<IUser>{
    return this.http.put<IUser>(ConfigService.createURL('users',id), userData);
  }
}
