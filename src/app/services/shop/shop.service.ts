import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router, UrlSerializer } from '@angular/router';
import { Observable } from 'rxjs';
import { IPagination } from 'src/app/models/IPagination';
import { IProduction, IProductionDiscont } from 'src/app/models/IProduction';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private setializer: UrlSerializer

  ) { }

  productionList(
    pagesize: number,
    first:number,
    _classification: string[]
  ): Observable<IPagination<IProduction[]>>{
    const tree = this.router.createUrlTree([''],{ queryParams: {classification: _classification} });
    const query = this.setializer.serialize(tree);
    return this.http.get<IPagination<IProduction[]>>(
      `${ConfigService.createURL('cakeList', 'production')}/${pagesize}/${first}${query.substring(1)}`
    );
  }
  productGetMany(ids: string[]): Observable<IProduction[]> {
    const tree = this.router.createUrlTree([''],{ queryParams: {ids} });
    const query = this.setializer.serialize(tree);
    return this.http.get<IProduction[]>(`${ConfigService.createURL('cakeList', 'production')}${query}`);
  }
  getDiscont(): Observable<IProductionDiscont[]> {
    return this.http.get<IProductionDiscont[]>(ConfigService.createURL('cakeList', 'production/discont'));
  }
}
