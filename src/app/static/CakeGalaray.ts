import { ICakeGalerey } from "../models/ICakeGalerey";

export const StaticCakeGalery: ICakeGalerey[] = [
  {cakeCode:301, fileName:'b_301.jpg', forEvent:'birthday'},
  {cakeCode:305, fileName:'b_305.jpg', forEvent:'birthday'},
  {cakeCode:310, fileName:'b_310.jpg', forEvent:'birthday'},
  {cakeCode:315, fileName:'b_315.jpg', forEvent:'birthday'},
  {cakeCode:331, fileName:'b_331.jpg', forEvent:'birthday'},

  {cakeCode:400, fileName:'k_400.jpg', forEvent:'corporate'},
  {cakeCode:401, fileName:'k_401.jpg', forEvent:'corporate'},
  {cakeCode:402, fileName:'k_402.jpg', forEvent:'corporate'},
  {cakeCode:403, fileName:'k_403.jpg', forEvent:'corporate'},
  {cakeCode:404, fileName:'k_404.jpg', forEvent:'corporate'},

  {cakeCode:120, fileName:'w_120.jpg', forEvent:'wedding'},
  {cakeCode:121, fileName:'w_121.jpg', forEvent:'wedding'},
  {cakeCode:122, fileName:'w_122.jpg', forEvent:'wedding'},
  {cakeCode:123, fileName:'w_123.jpg', forEvent:'wedding'},
  {cakeCode:124, fileName:'w_124.jpg', forEvent:'wedding'},
  {cakeCode:125, fileName:'w_125.jpg', forEvent:'wedding'},

];
