import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { FormsHelpler } from 'src/app/helplers/FormsHelpler';

@Component({
  standalone:true,
  selector: 'app-form-control',
  templateUrl: './form-control.component.html',
  styleUrls: ['./form-control.component.scss'],
  imports:[
    CommonModule,
    ReactiveFormsModule,
    DropdownModule
  ]
})
export class FormControlComponent extends FormsHelpler implements OnInit {
  private static idNum = 0;
  @Input() rows= 3;
  @Input() tag = 'input';
  @Input() fieldName: string;
  @Input() type: string = 'text';
  @Input() label: string;
  @Input() id: string;
  @Input() ddItem: {name:string, code:any}[] | null = null;
  @Input() formGroup: FormGroup;
 // @Output() ddClick = new EventEmitter<{name:string, code:any}>();
  constructor() { super(); }

  ngOnInit(): void {
    if (!this.id){
      this.id = 'fcW_' + FormControlComponent.idNum++;
    }
  }

}
