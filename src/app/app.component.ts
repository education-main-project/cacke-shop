import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from './services/users/users.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'cake-shop';
  constructor(private roter:Router, private userService: UsersService) {}
  ngOnInit(): void {
    this.userService.rescanUser();
  }
}
