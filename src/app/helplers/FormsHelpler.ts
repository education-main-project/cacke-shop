import { FormControl, FormGroup } from "@angular/forms";

export class FormsHelpler{
  errors(fG: FormGroup, fieldName:string): string {
    const fc:FormControl|null = (fG?.get(fieldName) as FormControl | null);
    if (fc!==null && fc.errors && (fc.dirty || fc.touched)){
      if (fc.errors['required']){
        return 'Поле необходимо заполнить'
      }
      if (fc.errors['minlength']){
        return `Длина должнабыть не меньше ${fc.errors['minlength'].requiredLength} символов, еще ${this.messageMapping(fc.errors['minlength'].requiredLength - fc.errors['minlength'].actualLength)}`
      }
      if (fc.errors['incorrectEmailAddress']){
        return 'Введите коректный email'
      }
      if (fc.errors['passDoesNotMatch']){
        return 'Пароли не совпадают'
      }
      if (fc.errors['passIsNotStrongEnough']){
        return 'Пароль должен содержать строчные и заглавные буквы, цифры, и -!$%^&*()_+|~=`{}\\:";\'<>?.,'
      }
      if (fc.errors['email']){
        return fc.errors['email'];
      }
      if (fc.errors['username']){
        return fc.errors['username'];
      }
      if (fc.errors['login']){
        return fc.errors['login'];
      }
      console.log("Не удалось определить ошибку", fc.errors);
      return "Имеются ошибки";
    }else{
      return '';
    }
  }
  isValid(fG: FormGroup, fieldName:string){
    const fc:FormControl|null = (fG?.get(fieldName) as FormControl | null);
    return fc===null || (fc.touched && (!fc.errors || !fc.errors[fieldName]));
  }
  private messageMapping(num:number):string{
    const vals=[
      'один символ',
      '# символa',
      '# символa',
      '# символa',
      '# символов'
    ];
    let cmpStr=vals[vals.length-1];
    if (num-1<vals.length-1){
      cmpStr=vals[num-1];
    }
    return cmpStr.replace('#',`${num}`);
  }
}
