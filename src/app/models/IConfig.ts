export interface IEndPoints {
  users: string,
  main: string,
  production: string,
  cakeList: string
}
export interface IConfig{
  serverProtocol:string,
  endPoints: IEndPoints,
  storeUser: boolean,
  token:{
    storageKey: string
  },
  user:{
    storageKey: string
  },
  cart:{
    storageKey: string
  },
  shopFilter:{
    storageKey: string
  }
}
