export interface IDeleteAnswer{
  status: string,
  deletedCount: number
}
