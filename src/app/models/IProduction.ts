/**
 * Классификация:
 *  торт, пироженное, дисерт, выпечка
 */
export type TPoductClassification =
  | 'торт'
  | 'пироженное'
  | 'десерт'
  | 'выпечка';
export interface IShortProduction {
  name: string;
  description: string;
  recipe: string;
  sloganPromotion: string;
  classification: TPoductClassification;
  imgPath: string;
  imgPath2: string;
  coast: number;
  diskont: number;
  weight: number;
}

export interface IProduction extends IShortProduction {
  _id: string;
}

export interface IProductionAList extends Pick<IProduction, '_id' | 'name' | 'imgPath' | 'classification' | 'coast' | 'diskont'> {}

export interface IProductionDiscont extends Pick<IProduction, '_id' | 'name' | 'sloganPromotion' | 'imgPath' | 'coast' | 'diskont'> {}

export type TTmpProdCntStore =  {prodId:string ,count:number, coast:number, diskont:number, isInCart: boolean};
