import { IProduction } from "./IProduction";

export interface ICartProduct extends Pick<IProduction, 'name' | 'coast' | 'diskont'> {
  productID: string,
  quantity: number
}

export interface ICardInfo {
  count: number,
  totalCoast: number
}
