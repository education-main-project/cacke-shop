export interface IPagination<DataType>{
    data: DataType,
    total: number,
    page: number,
    pageSize: number
}