import { TPoductClassification } from "./IProduction";

export interface IDDClassification {
  name: string,
  code: TPoductClassification
}
