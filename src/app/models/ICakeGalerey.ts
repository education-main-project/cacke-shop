type CakeEvents = 'birthday' | 'corporate' | 'wedding';
export interface ICakeGalerey {
  _id?:number;
  cakeCode: number;
  fileName: string;
  forEvent: CakeEvents;
}
