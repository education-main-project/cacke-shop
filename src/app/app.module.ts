import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { ConfigService } from './services/config/config.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RestInterceptor } from './services/interceptors/rest/rest.interceptor';
import { ReactiveFormsModule } from '@angular/forms';
import { MessageService } from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import { RECAPTCHA_SETTINGS, RecaptchaSettings, RecaptchaFormsModule, RecaptchaModule } from "ng-recaptcha14";

const globalSettings: RecaptchaSettings =
      { siteKey: '6LfQeQUqAAAAAHxOb4pD6wDxAIlPYdWoKqUxCGxl' };

registerLocaleData(localeRu, 'ru');


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastModule,
    RecaptchaModule,
    RecaptchaFormsModule

  ],
  providers: [
    ConfigService,
    {
      provide:APP_INITIALIZER,
      useFactory:initializeApp,
      deps: [ConfigService], multi: true
    },
    { provide: LOCALE_ID, useValue: 'ru' },
    { provide: HTTP_INTERCEPTORS, useClass: RestInterceptor, multi: true},
    MessageService,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: globalSettings
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


function initializeApp(config: ConfigService) {
  return () => config.loadPromise().then(() => {
    console.log('---CONFIG LOADED--', ConfigService.config)
  }).catch(err=>{
    console.log(err);
    throw new Error(err);
  });
}
