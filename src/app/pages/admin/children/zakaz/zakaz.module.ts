import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZakazRoutingModule } from './zakaz-routing.module';
import { ZakazComponent } from './zakaz.component';
import { ZakazListComponent } from './component/zakaz-list/zakaz-list.component';


@NgModule({
  declarations: [
    ZakazComponent,
    ZakazListComponent
  ],
  imports: [
    CommonModule,
    ZakazRoutingModule
  ]
})
export class ZakazModule { }
