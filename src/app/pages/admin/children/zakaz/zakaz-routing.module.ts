import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ZakazComponent } from './zakaz.component';
import { ZakazListComponent } from './component/zakaz-list/zakaz-list.component';

const routes: Routes = [
  {
    path:'',
    component:ZakazComponent,
    children:[
      {
        path:'',
        component:ZakazListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ZakazRoutingModule { }
