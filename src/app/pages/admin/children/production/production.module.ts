import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductionRoutingModule } from './production-routing.module';
import { ProductionComponent } from './production.component';
import { ProductionUpdateComponent } from './component/production-update/production-update.component';
import { ProductionFormComponent } from './component/production-form/production-form.component';
import { ProductionListComponent } from './component/production-list/production-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductionCreateComponent } from './component/product-create/production-create.component';
import { FormControlComponent } from 'src/app/components/form-control/form-control.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import {ToolbarModule} from 'primeng/toolbar';
import { ConfirmationService } from 'primeng/api';
import {ConfirmPopupModule} from 'primeng/confirmpopup';

@NgModule({
  declarations: [
    ProductionComponent,
    ProductionUpdateComponent,
    ProductionFormComponent,
    ProductionListComponent,
    ProductionCreateComponent
  ],
  imports: [
    CommonModule,
    ProductionRoutingModule,
    ReactiveFormsModule,
    FormControlComponent,
    TableModule,
    ButtonModule,
    PaginatorModule,
    ToolbarModule,
    ConfirmPopupModule,
  ],
  providers:[ConfirmationService]
})
export class ProductionModule { }
