import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { IProduction } from 'src/app/models/IProduction';
import { ProductionRestService } from 'src/app/services/production-rest/production-rest.service';

@Component({
  selector: 'app-production-update',
  templateUrl: './production-update.component.html',
  styleUrls: ['./production-update.component.scss']
})
export class ProductionUpdateComponent implements OnInit {
  toUpdate: IProduction | null = null;
  backPage = 0;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productionService: ProductionRestService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')!;
    this.backPage = +this.route.snapshot.paramMap.get('page')!;
    console.log('Back page', this.backPage);
    console.log("update id:", id);
    this.productionService.getProduct(id).subscribe({
      next: (dt)=>{
        this.toUpdate=dt;
      },
      error: (err: HttpErrorResponse)=>{
        if (Array.isArray(err.error)){
          this.messageService.add({severity:'error', summary:'Ошибка', detail:err.error[0].message});
        }else{
          console.error(err);
        }
      }
    });
  }
  onDone(product: IProduction){
    console.log("Добавлен продукт", product);
    console.log("Возврат на ",this.backPage)
    this.router.navigate(['/admin/production/list', this.backPage]);
  }
}
