import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IProduction, TPoductClassification } from 'src/app/models/IProduction';
import { SubTitleService } from 'src/app/services/sub-title/sub-title.service';

@Component({
  selector: 'app-product-create',
  templateUrl: './production-create.component.html',
  styleUrls: ['./production-create.component.scss']
})
export class ProductionCreateComponent implements OnInit {
  productForm: FormGroup;
  constructor(
    private subTitleService: SubTitleService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.subTitleService.setSubTitle("создать новый продукт");
    this.productForm = new FormGroup({
      name: new FormControl<string>('', {validators:[Validators.required]}),
      description: new FormControl<string>('', {validators:[Validators.required]}),
      classification: new FormControl<TPoductClassification>('торт', {validators:[Validators.required]}),
      imgPath: new FormControl(),
      coast: new FormControl<number>(0)
    });
  }
  onDone(product: IProduction){
    console.log("Добавлен продукт", product);
    this.router.navigate(['/admin/production',100000]);
  }
}
