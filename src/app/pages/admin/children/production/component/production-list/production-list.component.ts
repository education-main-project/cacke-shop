import { IProductionAList, IProduction } from 'src/app/models/IProduction';
import { SubTitleService } from './../../../../../../services/sub-title/sub-title.service';
import { Component, OnInit } from '@angular/core';
import { ProductionRestService } from 'src/app/services/production-rest/production-rest.service';
import { map, tap } from 'rxjs';
import { IPagination } from 'src/app/models/IPagination';
import { ConfirmationService, FilterMetadata, LazyLoadEvent, MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-production-list',
  templateUrl: './production-list.component.html',
  styleUrls: ['./production-list.component.scss']
})
export class ProductionListComponent implements OnInit {
  page=0;
  productList: IProductionAList[] = [];
  pageSize: number = 4;
  totalItemCount: number = 0;
  first: number = 0;
  loading = true;
  selectedProducts: IProduction[] = [];
  private filters: {
    [s: string]: FilterMetadata;
  } | undefined;
  ddOption = [
    {label: 'Пироженное', value: 'пироженное'},
    {label: 'Десерт', value: 'десерт'},
    {label: 'Торт', value: 'торт'},
    {label: 'Выпечка', value: 'выпечка'},
]

  get sp(){
    return JSON.stringify(this.selectedProducts);
  }
  constructor(
    private subTitleService: SubTitleService,
    private productionService: ProductionRestService,
    private router:Router,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private confirmService: ConfirmationService
  ) { }

  ngOnInit(): void {
    this.productList = [];
    this.page = +this.route.snapshot.paramMap.get('page')!;
    if (!this.page){
      this.page = 1;
    }
    this.first = (this.page - 1)*this.pageSize;
    console.log('Route page', this.page);
    this.subTitleService.setSubTitle("список");
  }
  private requestPage(pagesize: number, first: number ): void {
    this.pageSize = pagesize;
    this.first = first;
    this.productionService.listOfProduction(this.pageSize, this.first, this.filters).pipe(
      tap(data=>{
        console.log('От сервера', data);
        this.pageSize = data.pageSize;
        this.totalItemCount = data.total;
      }),
      map<IPagination<IProduction[]>, IProductionAList[]>( data=>{
        return data.data.map( ({
          _id,
          name,
          coast,
          imgPath,
          classification,
          diskont
        })=>{
          const rv:IProductionAList = {
            _id,
            name,
            coast,
            imgPath,
            classification,
            diskont
          }
          return rv;
        })
      })
    ).subscribe( data=>{
      this.productList = data;
      console.log(data);
      this.loading = false;
    });
  }
  loadCustomers(event: LazyLoadEvent){
    this.page = Math.floor(( event.first?event.first:0 )/this.pageSize) + 1;
    this.loading = true;
    this.filters = event.filters;
    this.requestPage(event.rows?event.rows:this.pageSize, event.first?event.first:0);
    this.router.navigate(['/admin/production/list',this.page],{ skipLocationChange:false,   replaceUrl:true });
    this.selectedProducts = [];
    console.log('lazy load event', event);
    console.log('lazy load page',this.page);
  }

  openNew():void{
    this.router.navigate(['/admin/production/create'])
  }

  deleteSelectedProducts(event:MouseEvent):void{
    console.log('delete', this.selectedProducts.map(it=>it._id));
    const names = this.selectedProducts.map(it=>`"${it.name}"`).join(', ');
    this.confirmService.confirm({
      target: event.target?event.target:undefined,
      message: `Удалить продукт${this.selectedProducts.length>1?'ы':''}: ${names} ?`,
      acceptLabel:'Да',
      rejectLabel:'Нет',
      icon: 'pi pi-exclamation-triangle',
      defaultFocus:'reject',
      accept: ()=>{
        this.selectedProducts.forEach(it=>{
          this.proceedDeletingProduct(it._id);
        });
        this.selectedProducts = [];
      }
    });

  }
  editProduct(id: string): void {
    console.log(id);
    this.router.navigate(['/admin/production/update', id, this.page]);
  }

  private proceedDeletingProduct(id: string){
    this.productionService.deleteOne(id).subscribe({
      next: dt=>{
        this.messageService.add({
          severity:'success',
          summary:'Выполнено',
          detail:'Одна запись удалена'
        });
        this.requestPage(this.pageSize, this.first);
      },
      error: err=>{
        console.log('Ошибка',err);
        this.messageService.add({
          severity:'error',
          summary:'Ошибка',
          detail:'Ошибка удаления записи'
        });
      }
    });
    console.log(`removing id:"${id}"`);
  }

  deleteProduct(event:MouseEvent, id: string, name: string = ''): void {
    if (id){
      this.confirmService.confirm({
        target: event.target?event.target:undefined,
        message: name?`Удалить продукт - "${name}"?`:'Удалить запись?',
        acceptLabel:'Да',
        rejectLabel:'Нет',
        icon: 'pi pi-exclamation-triangle',
        defaultFocus:'reject',
        accept: ()=>{ this.proceedDeletingProduct(id); }
      });
    }else{
      console.log(`removing bad id:"${JSON.stringify(id)}"`);
    }
  }
}
