import { HttpErrorResponse } from '@angular/common/http';
import { ProductionRestService } from './../../../../../../services/production-rest/production-rest.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormsHelpler } from 'src/app/helplers/FormsHelpler';
import { IProduction, TPoductClassification } from 'src/app/models/IProduction';
import { SubTitleService } from 'src/app/services/sub-title/sub-title.service';
import {IDDClassification} from 'src/app/models/IDDClassification';

@Component({
  selector: 'app-production-form',
  templateUrl: './production-form.component.html',
  styleUrls: ['./production-form.component.scss'],
})
export class ProductionFormComponent extends FormsHelpler implements OnInit {
  @Output() ondone = new EventEmitter<IProduction>();
  @Input() toUpdate: IProduction | null = null;
  productForm: FormGroup;
  ddOption: IDDClassification[]=[
    {name:'Пироженное', code:'пироженное'},
    {name:'Дeсерт', code:'десерт'},
    {name:'Торт', code:'торт'},
    {name:'Выпечка', code:'выпечка'},
  ];
  src_pic1:string | ArrayBuffer | null = '';
  src_pic2:string | ArrayBuffer | null = '';
  constructor(
    private subTitleService: SubTitleService,
    private productionService: ProductionRestService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subTitleService.setSubTitle(this.toUpdate?`обновить продукт "${this.toUpdate.name}"`:'создать новый продукт');
    if (this.toUpdate){
      this.productForm = new FormGroup({
        name: new FormControl<string>(this.toUpdate.name, {validators:[Validators.required]}),
        description: new FormControl<string>(this.toUpdate.description, {validators:[Validators.required]}),
        classification: new FormControl<TPoductClassification>(this.toUpdate.classification, {validators:[Validators.required]}),
        recipe: new FormControl(this.toUpdate.recipe),
        sloganPromotion: new FormControl(this.toUpdate.sloganPromotion),
        imgPath: new FormControl<string>(this.toUpdate.imgPath),
        imgPath2: new FormControl<string>(this.toUpdate.imgPath2),
        newFile: new FormControl(),
        newFile2: new FormControl(),
        coast: new FormControl<number>(this.toUpdate.coast),
        diskont: new FormControl<number>(this.toUpdate.diskont),
        weight: new FormControl<number>(this.toUpdate.weight)
      });
      this.src_pic1=this.toUpdate.imgPath;
      this.src_pic2=this.toUpdate.imgPath2;
    } else {
    this.productForm = new FormGroup({
      name: new FormControl<string>('', {validators:[Validators.required]}),
      description: new FormControl<string>('', {validators:[Validators.required]}),
      classification: new FormControl<TPoductClassification>('торт', {validators:[Validators.required]}),
      recipe: new FormControl(''),
      sloganPromotion: new FormControl(''),
      newFile: new FormControl(),
      newFile2: new FormControl(),
      coast: new FormControl<number>(0),
      diskont: new FormControl<number>(0),
      weight: new FormControl<number>(0.2)
    });
    }
  }

  selectFile(event: Event, fName: string): void {
    const el = event.currentTarget as HTMLInputElement;
    const fileList: FileList|null = el.files;
    console.log(event,fileList);
    if (fileList && fileList.length){
      const tmp: {[key: string]: any} = {};
      tmp[fName] = fileList[0];
      this.productForm.patchValue(tmp);
      if (fileList[0]) {
        const file = fileList[0];
        const reader = new FileReader();
        reader.onload = e => {
          if (fName === 'newFile'){
            this.src_pic1 = reader.result;
          }else{
            this.src_pic2 = reader.result;
          }
        }
        reader.readAsDataURL(file);
      }
    }
  }

  onSubmit(){
    console.log(this.productForm.getRawValue());
    const rawData = this.productForm.getRawValue();
    const formData = new FormData();
    if (typeof(rawData) === 'object'){
      for (let key in rawData){
        formData.append( key, rawData[key] );
      }
      if (this.toUpdate){
        this.productionService.updateProduct(this.toUpdate._id, formData).subscribe({
          next: answer=>{
            this.ondone.emit(answer);
          },
          error: (err: HttpErrorResponse)=>{

            console.log(err);
          }
        });
      }else{
        this.productionService.createProduct(formData).subscribe({
          next: answer=>{
            this.ondone.emit(answer);
          },
          error: (err: HttpErrorResponse)=>{

            console.log(err);
          }
        });
      }
    }
  }
}
