import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SubTitleService } from 'src/app/services/sub-title/sub-title.service';

@Component({
  selector: 'app-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.scss']
})
export class ProductionComponent implements OnInit, OnDestroy {
  title:string = "";
  private sTSubscription: Subscription;
  constructor(
    private subTitleService: SubTitleService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.sTSubscription = this.subTitleService.$subTitle.subscribe(newTitle=>{
      setTimeout(()=>{this.title = newTitle.charAt(0).toUpperCase() + newTitle.substring(1);});
    });
    this.authService.checkToken().subscribe({next: dt=>{
      console.log('Токен: ', dt);
    }})
  }
  ngOnDestroy(): void {
    this.sTSubscription.unsubscribe();
  }
}
