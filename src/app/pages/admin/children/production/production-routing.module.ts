import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductionComponent } from './production.component';
import { ProductionCreateComponent } from './component/product-create/production-create.component';
import { ProductionListComponent } from './component/production-list/production-list.component';
import { ProductionUpdateComponent } from './component/production-update/production-update.component';

const routes: Routes = [
  {
    path:'',
    component:ProductionComponent,
    children:[
      {
        path:'list/:page',
        component:ProductionListComponent
      },
      {
        path:'list',
        component:ProductionListComponent
      },
      {
        path:'create',
        component:ProductionCreateComponent
      },
      {
        path:'update/:id',
        component:ProductionUpdateComponent
      },
      {
        path:'update/:id/:page',
        component:ProductionUpdateComponent
      },
      {
        path:'**',
        redirectTo:'list'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionRoutingModule { }
