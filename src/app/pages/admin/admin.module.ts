import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { MenubarModule } from 'primeng/menubar';
import { Title } from '@angular/platform-browser';
import { DropdownModule } from 'primeng/dropdown';


@NgModule({
  declarations: [
    AdminComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MenubarModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
  ],
  providers:[
    Title
  ]

})
export class AdminModule { }
