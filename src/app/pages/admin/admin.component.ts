import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { take } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  mainMenu: MenuItem[] = [
    {
      label:'Пернйти на сайт',
      routerLink:['/'],
      routerLinkActiveOptions: { exact: true }
    },
    {
      label:'Заказы',
      routerLinkActiveOptions: { exact: true },
      items:[
        {
          label:'Список',
          routerLink:['/admin'],
          routerLinkActiveOptions: { exact: true }
        }
      ]
    },
    {
      label:'Продукция',
      items:[
        {
          label:'Список продукции',
          routerLink:['/admin/production'],
          routerLinkActiveOptions: { exact: true }
        },
        {
          label:'Добавить продукт',
          routerLink:['/admin/production/create'],
          routerLinkActiveOptions: { exact: true }
        }
      ]
    },
  ]

  constructor(
    private title: Title,
    private router: Router,
    private usersService: UsersService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.title.setTitle('Магазин тортов - админка');
    this.usersService.$user.pipe(take(1)).subscribe(
      uData=>{
        if (uData){
          console.log('has user',uData);
          this.mainMenu.push({
            label:`(${uData.realname})`,
            icon:'pi pi-user',
            items:[
              {
                label:'Выйти',
                icon:'pi pi-sign-out',
                command:()=>{
                  this.authService.logout();
                  this.router.navigate(['/']);
                }
              }
            ]
          });
        }
      }
    );
  }
}
