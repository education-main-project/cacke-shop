import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path:'',
    component: AdminComponent,
    children:[
      {
        path:'',
        loadChildren: () => import('./children/zakaz/zakaz.module').then(m=>m.ZakazModule)
      },
      {
        path:'production',
        loadChildren: () => import('./children/production/production.module').then(m=>m.ProductionModule)
      }
    ]
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
