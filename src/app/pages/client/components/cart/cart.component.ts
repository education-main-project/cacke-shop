import { AfterContentChecked, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { RecaptchaErrorParameters } from 'ng-recaptcha14';
import { ConfirmationService } from 'primeng/api';
import { Subscription, filter, of, switchMap, take, withLatestFrom } from 'rxjs';
import { ICardInfo, ICartProduct } from 'src/app/models/ICart';
import { IProduction } from 'src/app/models/IProduction';
import { CartService } from 'src/app/services/cart/cart.service';
import { NeedLoginService } from 'src/app/services/need-login/need-login.service';
import { ShopService } from 'src/app/services/shop/shop.service';
import { UsersService } from 'src/app/services/users/users.service';

interface ICartDats extends ICartProduct {
  product: IProduction
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy, AfterContentChecked {
  @Input() showCart: boolean = false;

  @Input() displayModal: boolean = false;
  @Output() displayModalChange = new EventEmitter<boolean>();
  @Output() refreshRequest = new EventEmitter<void>();
  private cartInfoSubscription: Subscription;
  private cartDataSubscription: Subscription;
  private routerSubscription: Subscription;
  showSubmit = false;
  needRefresh = false;
  cartInfo: ICardInfo = {totalCoast:0, count:0};
  cartData: ICartProduct[] = [];
  constructor(
    private cartService: CartService,
    private shopService: ShopService,
    private confirmService: ConfirmationService,
    private needLoginService: NeedLoginService,
    private userService: UsersService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.needRefresh = false;
    this.showSubmit = false;
    this.cartInfoSubscription = this.cartService.$cartInfoSubject.subscribe(dt=>{
      this.cartInfo = dt;
    });
    this.routerSubscription = this.router.events.pipe(filter(ev=>ev instanceof NavigationStart )).subscribe(ev=>{
      this.closeDialog();
    });
    this.cartDataSubscription = this.cartService.$cartSubject.pipe(
      switchMap(dt=>this.shopService.productGetMany(dt.map(it=>it.productID))
),
      withLatestFrom(this.cartService.$cartSubject),
      switchMap(([dt1, dt2])=>
        of(dt2.map(dt1It=>{
          return <ICartDats><unknown>{ ...dt1It, product: dt1.find(dt2It => dt2It._id === dt1It.productID) }
        }))
      ),
    ).subscribe(dt=>{
      this.cartData = dt;
      console.log('sbscr',dt);
    });
  }
  ngOnDestroy(): void {
    this.cartInfoSubscription.unsubscribe();
    this.cartDataSubscription.unsubscribe();
    this.routerSubscription.unsubscribe();
  }
  ngAfterContentChecked(): void {
    if (!this.cartData.length){
      this.displayModal = false;
    }

  }
  closeDialog(update: boolean = false): void {
    this.displayModal = false;
    if (update || this.needRefresh){
      this.refreshRequest.emit();
    }
  }
  addClick(): void {
    this.userService.$user.pipe(take(1)).subscribe(user=>{
      console.log('user',user);
      if (!user){
        this.needLoginService.doLogin();
      }else{
        this.closeDialog();
      }
    })
  }
  cartCountChange(value: number, id:string){
    this.cartService.updateQuantityById(id, value).then(dt=>{
      console.log(dt);
    }).catch(err=>{
      console.log(err);
    });
  }
  clearCart(): void {
    this.confirmService.confirm({
      acceptLabel:'Да',
      rejectLabel:'Нет',
      acceptButtonStyleClass:'p-button-danger',
      header:'Внимание!',
      message:`Очистить корзину?`,
      accept: () =>{
        this.cartService.clear().then(dt=>{
          this.closeDialog(false);
        }).catch(console.error);
      }
    });
  }
  removeOne(id:string, name: string): void {
    this.confirmService.confirm({
      acceptLabel:'Да',
      rejectLabel:'Нет',
      acceptButtonStyleClass:'p-button-danger',
      header:'Внимание!',
      message:`Удалить "${name}"?`,
      accept: () =>{
        this.cartService.removeById(id).then(dt=>{
          console.log(dt);
        }).catch(console.error);
      }
    });
  }
  percent(coast:number,discont:number): number{
    if (discont>0){
      return Math.round((coast - (coast/100)*discont)*100)/100;
    }else{
      return coast;
    }
  }
  get productCount(): number{
    return this.cartData.reduce((acc,it)=>acc+it.quantity,0);
  }
  public resolved(captchaResponse: string): void {
    this.showSubmit = true;
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  public onError(errorDetails: RecaptchaErrorParameters): void {
    this.showSubmit = false;
    console.log(`reCAPTCHA error encountered; details:`, errorDetails);
  }

}
