import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './client.component';
import { ForbiddenComponent } from './children/forbidden/forbidden.component';

const routes: Routes = [
  {
    path:'',
    component: ClientComponent,
    children:[
      {
        path:'',
        loadChildren:()=>import('./children/main-page/main-page.module').then(m=>m.MainPageModule)
      },
      {
        path:'shop/:page',
        loadChildren:()=>import('./children/shop/shop.module').then(m=>m.ShopModule)
      },
      {
        path:'shop',
        loadChildren:()=>import('./children/shop/shop.module').then(m=>m.ShopModule)
      },
      {
        path:'recipes',
        loadChildren:()=>import('./children/recipes/recipes.module').then(m=>m.RecipesModule)
      },
      {
        path: 'users',
        loadChildren: () => import('./children/users/users.module').then(m=>m.UsersModule)
      },
      {
        path: 'forbidden',
        component:ForbiddenComponent
      },
      {
        path: 'gallery',
        loadChildren: () => import('./children/cake-gallery/cake-gallery.module').then(m=>m.CakeGalleryModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
