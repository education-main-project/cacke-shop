import {  Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { LazyLoadEvent } from 'primeng/api';
import { Subscription, filter, take } from 'rxjs';
import { ICartProduct } from 'src/app/models/ICart';
import { IProduction, TTmpProdCntStore } from 'src/app/models/IProduction';
import { CartService } from 'src/app/services/cart/cart.service';
import { ShopFilterStoreService } from 'src/app/services/shop-filter-store/shop-filter-store.service';
import { ShopService } from 'src/app/services/shop/shop.service';


@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})

export class ShopComponent implements OnInit, OnDestroy {
  production: IProduction[] = [];
  private TmpProdCntStore: TTmpProdCntStore[] = [];
  private cartSubscription: Subscription;
  selectedValues: string[] = ['торт', 'пироженное', 'десерт' ,'выпечка'];
  page=0;
  pageSize: number = 4;
  totalItemCount: number = 0;
  first: number = 0;
  loading = true;
  blocklazy = false;

  private routerEvenSubscription: Subscription;
  constructor(
    private shopService: ShopService,
    private cartService: CartService,
    private router: Router,
    private route: ActivatedRoute,
    private filterService: ShopFilterStoreService,
    private title: Title
  ) { }

  ngOnInit(): void {
    //Установка заголовка
    this.title.setTitle("Магазин тортов - продажа.");
    //Устоновить текущую страницу
    this.setCurrentPageFromRoute();
    //Подписка на корзину
    this.cartSubscription = this.cartService.$cartSubject.subscribe(dt => {
      console.log('Cart storage has update:', dt);
      this.proceedWithStoredCart(dt);
    });
    //Подписка на окончание навигации
    this.routerEvenSubscription = this.router.events.pipe(
      filter(dt=>dt instanceof NavigationEnd )
    ).subscribe(
      dt=>{
        if (!this.loading){
          console.log('NavigationEnd - инициируем загрузку');
          this.reloadDataOnRouteChange();
        }else{
          console.log('NavigationEnd - загрузка занята ')
        }
      }
    );
    //Подписка на сохраненные фильтры
    this.filterService.$filter.pipe(take(1)).subscribe(dt=>{
      this.selectedValues = dt;
    });
   }

  ngOnDestroy(): void {
    this.cartSubscription.unsubscribe();
    this.routerEvenSubscription.unsubscribe();
  }

  private setCurrentPageFromRoute(): void {
    this.page = +this.route.snapshot.paramMap.get('page')!;
    if (!this.page){
      this.page = 1;
    }
    this.first = (this.page - 1)*this.pageSize;
    console.log('setCurrentPageFromRoute route page', this.page);
  }

  private reloadDataOnRouteChange(): void {
    console.log('reloadDataOnRouteChange');
    this.loading = true;
    this.blocklazy = true;
    this.setCurrentPageFromRoute();
    this.requestPage(this.pageSize, this.first);
  }

  loadCustomers(event: LazyLoadEvent){
    if (!this.blocklazy){
      this.page = Math.floor(( event.first?event.first:0 )/this.pageSize) + 1;
      this.loading = true;
      this.requestPage(event.rows?event.rows:this.pageSize, event.first?event.first:0);
      this.router.navigate(['/shop',this.page]);
    }
  }

  private proceedWithStoredCart(dt: ICartProduct[]): void {
    this.TmpProdCntStore = [];
    dt.forEach(it=>{
      this.TmpProdCntStore.push({
        count: it.quantity,
        prodId: it.productID,
        coast: it.coast,
        isInCart: true,
        diskont: it.diskont
      });
    });
  }
  private requestPage(pagesize: number, first: number ): void {
    this.blocklazy = true;
    this.pageSize = pagesize;
    this.first = first;
    console.log('requestPage - do request');
    this.shopService.productionList(pagesize,first, this.selectedValues).subscribe({
      next: dt=>{
        this.pageSize = dt.pageSize?dt.pageSize:4;
        this.totalItemCount = dt.total;
        this.production = [...dt.data];
        this.loading = false;
        this.blocklazy = false;
      }
    });
  }
  cartAddClick(id: string, name:string, coast: number, discont: number){
    const tmpItem = this.TmpProdCntStore.find(it=>it.prodId===id);
    if (tmpItem){
      this.cartService.addProduct(id, name, coast, tmpItem.count, discont).then(console.log).catch(console.error);
    }else{
      this.cartService.addProduct(id, name, coast, 1, discont).then(console.log).catch(console.error);
    }
  }
  cartCountChange(value: number, id:string){
    this.cartService.updateQuantityById(id, value).then(console.log).catch(err=>{
      const index = this.TmpProdCntStore.findIndex(it=>it.prodId===id);
      console.log(err);
      if (index>-1){
        this.TmpProdCntStore[index].count = value;
      }else{
        const product = this.production.find(it=>it._id === id);
        if (product){
          this.TmpProdCntStore.push({
            prodId:id,
            coast:product.coast,
            count:value,
            isInCart:false,
            diskont: product.diskont
          });
        }
      }
    });
  }
  getCountValById( id:string ): number {
    const item = this.TmpProdCntStore.find(it=>it.prodId === id);
    return item?item.count:0;
  }
  getCoastValById( id:string ): number {
    const item = this.TmpProdCntStore.find(it=>it.prodId === id);
    return item && item.isInCart ?(item.count* this.percent(item.coast, item.diskont)):0;
  }
  isInCart(id: string){
    const index = this.TmpProdCntStore.findIndex(it=>it.prodId===id);
    return index>-1 && this.TmpProdCntStore[index].isInCart?true:false;
  }
  checkBoxChange(){
    this.filterService.storeFilter(this.selectedValues);
    this.loading = true;
    this.requestPage(this.pageSize, this.first);
  }
  percent(coast:number,discont:number): number{
    if (discont>0){
      return Math.round((coast - (coast/100)*discont)*100)/100;
    }else{
      return coast;
    }
  }
 }
