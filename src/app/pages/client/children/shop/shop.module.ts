import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShopRoutingModule } from './shop-routing.module';
import { ShopComponent } from './shop.component';
import { DataViewModule } from 'primeng/dataview';
import {CardModule} from 'primeng/card';
import {ImageModule} from 'primeng/image';
import {ButtonModule} from 'primeng/button';
import {InputNumberModule} from 'primeng/inputnumber';
import { FormsModule } from '@angular/forms';
import { ChipModule } from 'primeng/chip';
import {CheckboxModule} from 'primeng/checkbox';
import {ToolbarModule} from 'primeng/toolbar';

@NgModule({
  declarations: [
    ShopComponent
  ],
  imports: [
    CommonModule,
    ShopRoutingModule,
    DataViewModule,
    CardModule,
    ImageModule,
    ButtonModule,
    InputNumberModule,
    FormsModule,
    ChipModule,
    CheckboxModule,
    ToolbarModule
  ]
})
export class ShopModule { }
