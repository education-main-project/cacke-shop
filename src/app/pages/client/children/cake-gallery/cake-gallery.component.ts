import { TreeNode } from 'primeng/api';
import { CakeGalereyService } from './../../../../services/cake-galerey/cake-galerey.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ICakeGalerey } from 'src/app/models/ICakeGalerey';
import { Subject, Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-cake-gallery',
  templateUrl: './cake-gallery.component.html',
  styleUrls: ['./cake-gallery.component.scss']
})
export class CakeGalleryComponent implements OnInit, OnDestroy {
  private groupSubject = new Subject<boolean>();
  private gsSubscription: Subscription;
  endableGroup: boolean = false;
  tList: TreeNode[];
  constructor(
    private cakeGallery: CakeGalereyService,
    private title: Title
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Магазин тортов - галерея");
    this.gsSubscription = this.groupSubject.subscribe(dtBool=>{
      this.cakeGallery.galerayList().subscribe(dt=>{
        this.tList = this.proceedData(dt,  dtBool);
      });
    });
    this.groupSubject.next(this.endableGroup);
  }
  ngOnDestroy(): void {
    this.gsSubscription.unsubscribe();
  }
  onCheckClick(event: any): void {
    console.log(event.checked);
    this.groupSubject.next(this.endableGroup);
  }
  private proceedData(dt: ICakeGalerey[], doGroup: boolean): TreeNode[]{
    if (Array.isArray(dt)){
      if (!doGroup){
        return dt.map(it=>{
          return {
            label:`${this.translate(it.forEvent)} №${it.cakeCode}`,
            data:it.fileName,
            type:'picture'
          };
        });
      }else{
        const treeNodeObj: TreeNode[] = [];
        return dt.reduce((acc,it)=>{
          const index = acc.findIndex(sit=>sit.label === this.translate(it.forEvent));
          if (index>-1){
              acc[index].children?.push({label:`№${it.cakeCode}`, data:it.fileName, type: 'picture'});
          }else{
            acc.push({
              label: this.translate(it.forEvent),
              children:[{label:`№${it.cakeCode}`, data:it.fileName, type: 'picture'}]
            });
          }
          return acc;
        }, treeNodeObj);
      }
    }else{
      return [];
    }
  }
  translate(s:string): string{
    switch(s){
      case 'birthday':
        return 'день рождения';
      case 'corporate':
        return 'корпоративный';
      case 'wedding':
        return 'свадебный';
      default:
        return s;
    }
  }
}
