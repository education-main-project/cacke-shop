import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CakeGalleryComponent } from './cake-gallery.component';

const routes: Routes = [
  {
    path:'',
    component: CakeGalleryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CakeGalleryRoutingModule { }
