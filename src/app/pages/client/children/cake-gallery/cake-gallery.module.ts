import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CakeGalleryRoutingModule } from './cake-gallery-routing.module';
import {CheckboxModule} from 'primeng/checkbox';
import {TreeModule} from 'primeng/tree';
import { FormsModule } from '@angular/forms';
import { CakeGalleryComponent } from './cake-gallery.component';
import {CardModule} from 'primeng/card';
import {ImageModule} from 'primeng/image';
import { CapitalDirective } from 'src/app/directives/capital/capital.directive';

@NgModule({
  declarations: [
    CakeGalleryComponent,
    CapitalDirective
  ],
  imports: [
    CommonModule,
    CakeGalleryRoutingModule,
    CheckboxModule,
    TreeModule,
    FormsModule,
    CardModule,
    ImageModule,
  ]
})
export class CakeGalleryModule { }
