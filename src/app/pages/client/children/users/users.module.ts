import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './components/users.component';
import { UserAddComponent } from './components/user-add/user-add.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ProgressBarModule } from 'primeng/progressbar';
import { LoginModule } from '../login/login.module';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { RecaptchaModule } from 'ng-recaptcha14';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    UsersComponent,
    UserAddComponent,
    UserLoginComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    DropdownModule,
    ProgressBarModule,
    LoginModule,
    RecaptchaModule,
    ButtonModule
   ],
  providers:[
  ]
})
export class UsersModule { }
