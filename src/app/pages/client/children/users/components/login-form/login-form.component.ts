import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TrimValidate } from 'src/app/validators/TrimValidate';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Output() submitOk = new EventEmitter<void>();
  @Output() cancelPress = new EventEmitter<void>();
  @Input() dialogMode = false;

  logForm: FormGroup;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.logForm = new FormGroup({
      username: new FormControl<string>('',{ validators:[TrimValidate(), Validators.required]}),
      password: new FormControl<string>('',{ validators:[Validators.required]}),
    });
  }
    onSubmit(){
      console.log('submit', this.logForm.getRawValue());
    this.authService.signin(this.logForm.getRawValue()).then( data=>{
      console.log('resp', data);
      this.logForm.reset({username:'', password:''});
      this.submitOk.emit();
    }).catch( err=>{
      console.log('Login error!',err);
      if (Array.isArray(err)){
        err.forEach(it=>{
          if (it.field){
            const fC = this.logForm.get(it.field) as FormControl<string> | null;
            if (fC){
              fC.setErrors({ login:it.message?it.message:"Содержит ошибку"});
            }else{
              console.log(`Login элемент "${it.field}" не найден`,it);
            }
          }else{
            console.log("Login неизвестная ошибка",it);
          }
          console.log(it.message);
        });
      }else{
        console.log('Ошибка:',err);
      }
    });

  }
  fC( fieldName:string ): FormControl|null{
    return this.logForm?.get(fieldName) as FormControl | null;
  }
  isInvalid(fName: string): boolean {
    const f= this.fC(fName);
    return !!f && f.invalid && f.touched;
  }
  get notEmpty(): boolean {
    const username = this.logForm.get('username') as FormControl | null;
    const password = this.logForm.get('password') as FormControl | null;
    if (!username || !password){
      return false;
    }else{
      const usernameValue = username.getRawValue() as string | null;
      const passwordValue = password.getRawValue() as string | null;
      if (!usernameValue || !passwordValue){
        return false;
      }else{
        if (!usernameValue.trim().length || !passwordValue.trim().length){
          return false;
        }else{
          return true;
        }
      }
    }
  }
  isValid(fName: string): boolean {
    const f= this.fC(fName);
    return !!f && f.valid && f.touched;
  }
  errors(fieldName:string): string {
    const fc:FormControl|null = (this.logForm?.get(fieldName) as FormControl | null);
    if (fc!==null && fc.errors && (fc.dirty || fc.touched)){
      if (fc.errors['required']){
        return 'Поле необходимо заполнить'
      }
        if (fc.errors['login']){
          return fc.errors['login'];
        }
      return "Имеются ошибки";
    }else{
      return '';
    }
  }

}
