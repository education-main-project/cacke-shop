import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { RecaptchaErrorParameters } from 'ng-recaptcha14';
import { MessageService } from 'primeng/api';
import { FormsHelpler } from 'src/app/helplers/FormsHelpler';
import { IRole, TRoles } from 'src/app/models/IUsers';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserRestService } from 'src/app/services/user-rest/user-rest.service';
import { EmailValidate } from 'src/app/validators/EmailValidate';
import { TrimValidate } from 'src/app/validators/TrimValidate';
import { PasswordConfirmViladator, PasswordStrength } from 'src/app/validators/password';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})

export class UserAddComponent extends FormsHelpler implements OnInit {

  regForm: FormGroup | null;
  leadingText = 'Регистрация';
  showSubmit = false;
  private _isUpdate = false;
  showChangePassword= false;
  rolesList: IRole[] = [
    {name:'Администратор', code:'admin'},
    {name:'Редактор', code: "moder"},
    {name:'Пользователь', code:'user'}
  ];
  private loadedUserId: string | undefined;
  constructor(
    private title:Title,
    private router: Router,
    private activeRouter:ActivatedRoute,
    private authService: AuthService,
    private userRestService: UserRestService,
    private messageService: MessageService
  ) { super(); }

  ngOnInit(): void {
    const uname =this.activeRouter.snapshot.paramMap.get('uname');
    this.showSubmit = false;
    if ( uname ){
      this.title.setTitle("Обновить пользователя");
      this.leadingText = 'Обновить данные пользователя';
      this._isUpdate = true;
      this.userRestService.getUser(uname).subscribe(dt=>{
        console.log('response:',dt);
        this.loadedUserId = dt._id;
        this.regForm = new FormGroup({
          username: new FormControl<string>(dt.username,{ validators:[TrimValidate(), Validators.required, Validators.minLength(3)], updateOn: 'blur'}),
          realname: new FormControl<string>(dt.realname,{ validators:[Validators.required, Validators.minLength(4)], updateOn: 'blur'}),
          email:new FormControl<string>(dt.email,{ validators:[TrimValidate(), Validators.required, EmailValidate()], updateOn: 'blur'}),
          role: new FormControl<TRoles>(dt.role)
        });
      });
    }else{
      this.title.setTitle("Магазин тортов - регистрация пользователя");
      this.regForm = new FormGroup({
        username: new FormControl<string>('',{ validators:[TrimValidate(), Validators.required, Validators.minLength(3)], updateOn: 'blur'}),
        realname: new FormControl<string>('',{ validators:[Validators.required, Validators.minLength(4)], updateOn: 'blur'}),
        email:new FormControl<string>('',{ validators:[TrimValidate(), Validators.required, EmailValidate()], updateOn: 'blur'}),
        password: new FormControl<string>('',{ validators:[Validators.required, Validators.minLength(6), PasswordStrength()], updateOn: 'blur'}),
        passwordRepeat: new FormControl<string>('',{ validators:[PasswordConfirmViladator('password'), Validators.required]}),
        role: new FormControl<TRoles>('user')
      });
    }
  }

  get isUpdate(): boolean {
    return this._isUpdate;
  }

  changePasswordClick(): void {
    this.regForm?.addControl(
      'password',
      new FormControl<string>('',{ validators:[Validators.required, Validators.minLength(6), PasswordStrength()], updateOn: 'blur'})
    );
    this.regForm?.addControl(
      'passwordRepeat',
      new FormControl<string>('',{ validators:[PasswordConfirmViladator('password'), Validators.required]})
    );
    this.showChangePassword = true;
  }

  hidePassword(): void {
    this.showChangePassword = false;
    this.regForm?.removeControl('password');
    this.regForm?.removeControl('passwordRepeat');
  }

  private proceedError(err: HttpErrorResponse){
    if (Array.isArray(err?.error)){
      err.error.forEach(it=>{
        console.log("Ошибка:", it);
        if (it.field && typeof(it.field)==='string'){
          const fC = this.regForm?.get(it.field) as FormControl<string> | null;
          if (fC){
            const tmp: {[index:string]:string} = {};
            tmp[it.field]=it.message;
            fC.setErrors(tmp);
          }else{
            console.log(`UserAdd элемент "${it.field}" не найден`,it);
          }
        }else{
          console.log("UserAdd неизвестная ошибка",it);
        }
        console.log(it.message);
      });
    }else{
      console.log("submit Error",err);
    }
  }

  private createUser(): void {
    const rawData = this.regForm?.getRawValue();
    console.log('create', rawData);
    this.authService.signup(rawData)
      .then(resp=>{
        console.log("submit ok");
        this.router.navigate( ['/'] );
      })
      .catch(_err=>{
        this.proceedError(_err as HttpErrorResponse);
      });
  }

  private updateUser(): void {
    const rawData = this.regForm?.getRawValue();
    const relogin = this.regForm?.get('password')?.dirty || this.regForm?.get('username')?.dirty
    if (this.loadedUserId){
      console.log(`update user with id="${this.loadedUserId}"`, rawData);
      this.userRestService.updateUser(this.loadedUserId, rawData).subscribe({
        next: answer=>{
            console.log("update ok");
            this.messageService.add({
              severity:'success',
              summary:'Сохранено',
              detail:'Пользователь обнавлён'+(relogin?' требуется повторная авторизация!':'.')
            });
            if (relogin) {
              this.authService.logout();
            }
            this.router.navigate( ['/'] );
          },
        error: err=>{
          this.proceedError(err as HttpErrorResponse);
        }
      })
    }else{
      console.log('Нет id пользователя');
    }
  }

  submit(){
    if (!this._isUpdate){
      this.createUser();
    }else{
      this.updateUser();
    }
  }
  public resolved(captchaResponse: string): void {
    this.showSubmit = !!captchaResponse;
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  public onError(errorDetails: RecaptchaErrorParameters): void {
    this.showSubmit = false;
    console.log(`reCAPTCHA error encountered; details:`, errorDetails);
  }
}
