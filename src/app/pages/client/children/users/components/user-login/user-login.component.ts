import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginFormComponent } from 'src/app/pages/client/children/users/components/login-form/login-form.component';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss'],
})
export class UserLoginComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private title: Title,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Магазин тортов - вход");
  }

  submitOk(){
    console.log('Роуте',this.router.routerState.snapshot.url);
    if (this.router.routerState.snapshot.url === '/users/login'){
      this.location.back();
    }
  }
}
