import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './components/users.component';
import { UserAddComponent } from './components/user-add/user-add.component';
import { UserLoginComponent } from './components/user-login/user-login.component';

const routes: Routes = [
  {
    path:'',
    component:UsersComponent,
    children:[
      {
        path:'login',
        component:UserLoginComponent,
        data:{hideCart:true}
      },
      {
        path:'register',
        component:UserAddComponent,
        data:{hideCart:true}
      },
      {
        path:'update/:uname',
        component:UserAddComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
