import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Observable, Subscription, concatMap, delay, finalize, from, mergeMap, of, skip } from 'rxjs';
import { ICartProduct } from 'src/app/models/ICart';
import { IProductionDiscont, TTmpProdCntStore } from 'src/app/models/IProduction';
import { CartService } from 'src/app/services/cart/cart.service';
import { ShopService } from 'src/app/services/shop/shop.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit, OnDestroy {
  info: IProductionDiscont;
  hidePic = true;
  private TmpProdCntStore: TTmpProdCntStore[] = [];
  private cartSubscription: Subscription;
  private diskontSubscription: Subscription;
  private isRun = false;
  private discontData: IProductionDiscont[] = [];
  constructor(
    private title: Title,
    private shopService: ShopService,
    private cartService: CartService,
  ) { }
  ngOnInit(): void {
    this.title.setTitle("Магазин тортов - главная");
    this.shopService.getDiscont().subscribe(
      dt=>{
        console.log(dt);
        this.info = dt[0];
        this.hidePic = false;
        this.isRun = true;
        this.discontData =dt;
        this.diskont();
      }
    );
    //Подписка на корзину
    this.cartSubscription = this.cartService.$cartSubject.subscribe(dt => {
      console.log('Cart storage has update:', dt);
      this.proceedWithStoredCart(dt);
    });
  }

  ngOnDestroy(): void {
    this.isRun = false;
    this.cartSubscription.unsubscribe();
    if(this.diskontSubscription){
      this.diskontSubscription.unsubscribe();
    }
  }
  private diskont(_skip:number = 1): void {
    if (this.diskontSubscription){
      this.diskontSubscription.unsubscribe();
    }
    if (this.isRun){
    this.diskontSubscription = of(this.discontData).pipe(
        mergeMap((dt: any)=>from<Observable<IProductionDiscont>>(dt)),
        skip(_skip),
        concatMap((dt:IProductionDiscont)=> {
          this.hidePic = true;
          console.log('in 1000',this.hidePic);
          return of(dt).pipe(
            delay(700),
            concatMap((dt:IProductionDiscont)=> {
              console.log('wait 3000',this.hidePic);
              return of(dt).pipe(
                delay(5000),
                concatMap((dt:IProductionDiscont)=> {
                  this.hidePic = false;
                  console.log('out 1000',this.hidePic);
                  return of(dt).pipe(
                    delay(700),
                    concatMap((dt:IProductionDiscont)=> {
                      this.info = dt;
                      console.log('change',this.hidePic);
                      return of(dt).pipe(delay(250))
                    }),
                  )
                }),
              )
            }),
          )
        }),
        finalize(()=>{
          console.log('end! restart');
          this.diskont(0);
        })
      ).subscribe();
    }
  }
  private proceedWithStoredCart(dt: ICartProduct[]): void {
    dt.forEach(it=>{
      const tmpItIndex = this.TmpProdCntStore.findIndex(tIt=>tIt.prodId === it.productID);
      if (tmpItIndex>-1){
        this.TmpProdCntStore[tmpItIndex].count = it.quantity;
        this.TmpProdCntStore[tmpItIndex].isInCart = true;
      }else{
        this.TmpProdCntStore.push({
          count: it.quantity,
          prodId: it.productID,
          coast: it.coast,
          diskont: it.diskont,
          isInCart: true
        });
      }
    });
  }

  cartAddClick(id: string, name:string, coast: number, discont: number){
    const tmpItem = this.TmpProdCntStore.find(it=>it.prodId===id);
    if (tmpItem){
      this.cartService.addProduct(id, name, coast, tmpItem.count, discont).then(console.log).catch(console.error);
    }else{
      this.cartService.addProduct(id, name, coast, 1, discont).then(console.log).catch(console.error);
    }
  }

  isInCart(id: string){
    const index = this.TmpProdCntStore.findIndex(it=>it.prodId===id);
    return index>-1 && this.TmpProdCntStore[index].isInCart?true:false;
  }
  percent(coast:number,discont:number): number{
    if (discont>0){
      return Math.round((coast - (coast/100)*discont)*100)/100;
    }else{
      return coast;
    }
  }

}
