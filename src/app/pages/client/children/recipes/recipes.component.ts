import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IProduction } from 'src/app/models/IProduction';
import { RecipesRestService } from 'src/app/services/recipes-rest/recipes-rest.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit {
  production: IProduction[] = [];
  constructor(
    private recipesService: RecipesRestService,
    private title: Title
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Магазин тортов - рецепты");
    this.recipesService.getRecipes().subscribe({
      next: dt=>{
        this.production = dt;
        console.log(dt);
      }
    });
  }

}
