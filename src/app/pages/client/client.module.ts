import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { MenubarModule } from 'primeng/menubar';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { Title } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginModule } from './children/login/login.module';
import { CartComponent } from './components/cart/cart.component';
import {TableModule} from 'primeng/table';
import {InputNumberModule} from 'primeng/inputnumber';
import {ToolbarModule} from 'primeng/toolbar';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { RecaptchaModule } from 'ng-recaptcha14';

@NgModule({
  declarations: [
    ClientComponent,
    CartComponent,
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    MenubarModule,
    DialogModule,
    ButtonModule,
    ReactiveFormsModule,
    LoginModule,
    TableModule,
    InputNumberModule,
    FormsModule,
    ToolbarModule,
    ConfirmDialogModule,
    RecaptchaModule,
  ],
  providers:[
    Title,
    ConfirmationService
  ],
})
export class ClientModule { }
