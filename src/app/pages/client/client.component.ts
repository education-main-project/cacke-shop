import { ICardInfo } from './../../models/ICart';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, ActivationStart, Router } from '@angular/router';
import {MenuItem} from 'primeng/api';
import { Subject, Subscription, filter, map, take, takeUntil, tap } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { NeedLoginService } from 'src/app/services/need-login/need-login.service';
import { UsersService } from 'src/app/services/users/users.service';
@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit, OnDestroy {
  showLogin:boolean = false;
  canShowCart = true;
  mainMenu: MenuItem[] = [
    {
      label:'Главная',
      routerLink:['/'],
      routerLinkActiveOptions: { exact: true }
    },
    {
      label:'Галерея тортов',
      routerLink:['/gallery']
    },
    {
      label:'Рецепты тортов',
      routerLink:['/recipes']
    },
    {
      label:'Магазин',
      routerLink:['/shop']
    },
    {
      label:'О нас'
    },
  ];
  private destr = new Subject<void>();
  private needLoginSubscription: Subscription;
  private userSubscription: Subscription;
  private cartInfoSubscription: Subscription;
  cartInfo: ICardInfo = {totalCoast:0, count:0};
  showRoute = true;
  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private cartInfoService: CartService,
    private needLoginService: NeedLoginService
  ) { }
  ngOnInit(): void {
    this.canShowCart = !this.reqursiveFindAsaidData(this.route.snapshot,'hideCart');
    this.router.events.pipe(
      filter(ev=>ev instanceof ActivationStart),
      map(ev=>(ev as ActivationStart).snapshot.data),
      takeUntil(this.destr),
    ).subscribe(data=>{
      this.canShowCart = !data['hideCart'];
    })
    this.needLoginSubscription = this.needLoginService.$needLogin.subscribe(()=>{
      this.showLogin = true;
    });
    this.userSubscription = this.usersService.$user.subscribe(
      uData=>{
        this.checkRedirect();
        this.mainMenu = [... this.mainMenu.slice(0, this.mainMenu.length-1)];
        if (uData){
          console.log('has user',uData);
          this.mainMenu.push({
            label:`(${uData.realname})`,
            icon:'pi pi-user',
            items:[
              {
                label:'Выйти',
                icon:'pi pi-sign-out',
                command:()=>{
                  this.authService.logout();
                  this.router.navigate(['/']);
                }
              }
            ]
          });
          if (this.authService.isAdmin){
            this.mainMenu[this.mainMenu.length-1].items?.push({
              icon:'pi pi-cog',
              label:'Админка',
              routerLink:['/admin'],
            });
          }else{
            this.mainMenu[this.mainMenu.length-1].items?.push({
              icon:'pi pi-cog',
              label:'Аккаунт',
              routerLink:['/users/update',uData.username],
            });
          }
        }else{
          console.log('no user', uData);
          this.mainMenu.push({
            label:"Войти",
            command:()=>{
              console.log("click");
              this.showLogin = true;
            }
          });
        }
      }
    );
    this.cartInfoSubscription = this.cartInfoService.$cartInfoSubject.subscribe(dt=>{
      this.cartInfo = dt;
    });
  }

  ngOnDestroy(): void {
    this.destr.next();
    this.destr.complete();
    this.cartInfoSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
    this.needLoginSubscription.unsubscribe();
  }
  private checkRedirect(): void {
      const url = this.router.url;
      console.log('url',url);
      if (url === '/users/register'
        || url === '/users/login'
      ){
        this.usersService.$user.pipe(take(1)).subscribe(user=>{
          if (user){
            console.log('has user redirect');
            if (user.role==='admin'){
              console.log('to admin');
              this.router.navigate(['/admin']);
            }else{
              console.log('to main');
              this.router.navigate(['/']);
            }
          }
        });
      }
    }
  closeLoginDialog(): void {
    this.showLogin = false;
  }
  private reqursiveFindAsaidData(curentSnapshot:ActivatedRouteSnapshot, searchProp:string): boolean {
    if (curentSnapshot.data[searchProp]){
      return true;
    }else{
      if (Array.isArray(curentSnapshot.children)){
        let result = false;
        curentSnapshot.children.every(item=>{
          result = this.reqursiveFindAsaidData(item, searchProp);
          return !result;
        });
        return result;
      }else{
        return false;
      }
    }
  }
  refresh(): void {
    console.log('refresh request');
    this.showRoute = false;
    setTimeout(()=>{
      location.reload();
      this.showRoute = true;
    });
  }
}
