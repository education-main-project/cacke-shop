import { AfterViewInit, Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appCapital]'
})
export class CapitalDirective implements OnInit, AfterViewInit{
  constructor(private el: ElementRef) { }
  ngOnInit(): void {

  }
  ngAfterViewInit(): void {
    const el = this.el.nativeElement;
    const text: string=el.textContent;
    if (text.length){
      el.textContent = text.charAt(0).toUpperCase() + text.substring(1);
      console.log('appCapital: ', text, el.textContent);
    }
  }
}
